package com.sinan.dpviewer.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.ShowViewHandler;

import com.sinan.dpviewer.astparser.ASTProcessor;
import com.sinan.dpviewer.patterns.PatternProcessor;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class DPViewCommandHandler extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public DPViewCommandHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        
        IStructuredSelection selection = (IStructuredSelection) HandlerUtil
                .getActiveMenuSelection(event);
        Object selectedProject = selection.getFirstElement();
        IJavaProject javaProject = null;
        
        if (selectedProject instanceof IJavaProject) {
        	javaProject = (IJavaProject) selectedProject;

        }
        else if (selectedProject instanceof IProject)
        {
        	IProject iproject = (IProject) selectedProject;
        	try {
				if (iproject.isNatureEnabled("org.eclipse.jdt.core.javanature")) 
				{
					javaProject = (IJavaProject) JavaCore.create(iproject);
				}
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }
        
        final IJavaProject finalJavaProject = javaProject;
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(window.getShell());
        try {
			dialog.run(true, true, new IRunnableWithProgress(){
				
			    public void run(IProgressMonitor monitor) {
			        monitor.beginTask("Calculating dependency patterns for project " + finalJavaProject.getElementName() + "...", 100000);
			        try {
				        // calculate dependency relations through eclipse jdt ast parser
						ASTProcessor astProcessor = new ASTProcessor(finalJavaProject, monitor);
						astProcessor.process();
						
						// if AST has changed
						if (ASTProcessor.hasClassListChangedLastRun()) {
					        // calculate dependency patterns
					        PatternProcessor dpProcessor = new PatternProcessor(ASTProcessor.getClassList(), ASTProcessor.getEdgeCount(), monitor);
					        dpProcessor.process();
						}
				        
				        monitor.done();					
					} catch (InterruptedException e) {
						monitor.done();
						e.printStackTrace();
					}
			        
			        monitor.subTask("");
			    }
			});
		} catch (InvocationTargetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
		try {
			if (dialog.getReturnCode() == ProgressMonitorDialog.OK) {
				window.getActivePage().showView("com.sinan.dpviewer.views.dpview");
			}
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
