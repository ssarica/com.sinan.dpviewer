/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sinan.dpviewer.patterns;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sinan
 */
public class HubClassFinder implements DependencyPatternClassFinder {

	public static float DEFAULT_DEPENDENCY_PERCENTAGE = 0.02f;
	
    private Collection<Clazz> classList;
    private Collection<Clazz> hubClassList;
    private int edgeCount;
    private float dependencyPercentage;

    public HubClassFinder(Collection<Clazz> classList, int edgeCount, float dependencyPercentage) {
        this.classList = classList;
        this.edgeCount = edgeCount;
        this.dependencyPercentage = dependencyPercentage;
        this.hubClassList = new ArrayList<Clazz>();
    }

    @Override
    public Collection<Clazz> find() {

        hubClassList.clear();
        for (Clazz klass : classList) {
            float depPer = (float)klass.getOutList().size() / edgeCount;
            if (depPer > dependencyPercentage) {
                klass.setType(Clazz.HUB);
                hubClassList.add(klass);
            }
        }

        return hubClassList;
    }

}
