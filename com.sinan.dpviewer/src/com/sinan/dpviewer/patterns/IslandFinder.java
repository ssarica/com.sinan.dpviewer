package com.sinan.dpviewer.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.sinan.dpviewer.util.Logger;

import net.sf.javaml.clustering.mcl.MarkovClustering;
import net.sf.javaml.clustering.mcl.SparseMatrix;
import net.sf.javaml.clustering.mcl.SparseVector;

public class IslandFinder {
	
	private static SparseMatrix sparseMatrix;
	private static HashMap<Integer, Clazz> classDict;
	
	public IslandFinder(Collection<Clazz> classList, int edgeCount) {
		SparseMatrix sm = new SparseMatrix();
		classDict = new HashMap<Integer, Clazz>();
		
		ArrayList<Clazz> lst = new ArrayList<Clazz>(classList);
		Collections.sort(lst, new Comparator<Clazz>() {

			@Override
			public int compare(Clazz arg0, Clazz arg1) {
				if (arg0.getTag() > arg1.getTag())
					return 1;
				if (arg0.getTag() < arg1.getTag())
					return -1;
				return 0;
			}
		});
        
		for (int i = 0; i < lst.size(); i++) {
			Clazz klass = lst.get(i);
			classDict.put(klass.getTag(), klass);
			//Logger.log("SparseVector - klass - " + klass.getName() + " " + klass.getTag());			
			//for (Clazz c : klass.getOutList()) {
				//Logger.log("SparseVector - " + c.getName() + " " + c.getTag());
			//}
		}        

		for (int i = 0; i < lst.size(); i++) {
			Clazz klass = lst.get(i);
			SparseVector sv = new SparseVector();
			for (Clazz c : klass.getOutList()) {
				sv.put(c.getTag(), 1.0);
			}

			sm.add(klass.getTag(), sv);
		}

        sparseMatrix = sm;
	}
	
	public Collection<Pattern> cluster() {
		// http://micans.org/mcl/man/mcl.html
		// This program implements mcl, a cluster algorithm for graphs. A single
		// parameter controls the granularity of the output clustering, namely
		// the -I inflation option described further below. In standard usage of
		// the program this parameter is the only one that may require changing.
		// By default it is set to 2.0 and this is a good way to start.
		MarkovClustering mc = new MarkovClustering();
		SparseMatrix clusters = mc.run(sparseMatrix, 0.5, 1.4, 1.0, 0.01);
		
		Logger.log("IslandFinder sparseMatrix: " + sparseMatrix);
		Logger.log("IslandFinder clusters: " + clusters);
		
		Set<Pattern> patterns = new HashSet<Pattern>();
		//Set<Clazz> nonClassifieds = new HashSet<Clazz>();
		
		int iTag = 1;
		for (int i = 0; i < clusters.size(); i++) {
			SparseVector sv = clusters.get(i);
			Clazz cl = classDict.get(i);
			Set<Clazz> clzes = new HashSet<Clazz>();
			clzes.add(cl);
			for (int k : sv.keySet()) {
				Clazz c = classDict.get(k);
				// if clazz is already classified, don't add to island
				if (c.getType() == Clazz.NONCLASSIFIED) {
					clzes.add(c);
				} else {
					Pattern p = new Pattern(c.getName(), c.getType(), c);
					p.setTag(c.getTag());
					patterns.add(p);
				}
			}

			if (clzes.size() > 8) {
				Pattern p = new Pattern("I" + iTag, Clazz.ISLAND, clzes);
				p.setTag(Pattern.ISLAND_OFFSET + iTag);
				patterns.add(p);
				iTag++;
			} 
//			else {
//				nonClassifieds.addAll(clzes);
//			}
		}
		
//		Pattern p = new Pattern("Island Nonclassfieds", Clazz.ISLAND, nonClassifieds);
//		patterns.add(p);
		
		return patterns;
	}

}
