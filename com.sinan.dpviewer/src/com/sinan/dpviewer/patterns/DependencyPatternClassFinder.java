/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sinan.dpviewer.patterns;

import java.util.Collection;

/**
 *
 * @author sinan
 */
public interface DependencyPatternClassFinder {

    public Collection<Clazz> find();

}
