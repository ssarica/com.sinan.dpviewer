package com.sinan.dpviewer.patterns;

import java.util.HashSet;
import java.util.Set;

public class Pattern {
	
	private int type = 0; //non-classified
	private String name;
	
	// island offset should be 10000
	public static int ISLAND_OFFSET = 10000;
	// bridge offset should be 20000
	public static int BRIDGE_OFFSET = 20000;
	private int tag;
	
	private Clazz clz;
	private Set<Clazz> clzes;
	
	private Set<Pattern> inSet;
	private Set<Pattern> outSet;
	
	public Pattern(String name, int type, Clazz clz) {
		this.name = name;
		this.type = type;
		this.clz = clz;
		inSet = new HashSet<Pattern>();
		outSet = new HashSet<Pattern>();
	}
	
	public Pattern(String name, int type, Set<Clazz> clzes) {
		this.name = name;
		this.type = type;
		this.clzes = clzes;
		inSet = new HashSet<Pattern>();
		outSet = new HashSet<Pattern>();	
	}
	
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Pattern) {
            if (this.name == null ? ((Pattern) obj).getName() == null
                    : this.name.equals(((Pattern) obj).getName())) {
                return true;
            }
        }
        return super.equals(obj);
    }	
    
    @Override
    public int hashCode() {
        int hash = 6;
        hash = 28 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }    
	
	public Clazz getClazz() {
		return clz;
	}
	
	public Set<Clazz> getClazzes() {
		return clzes;
	}
	
	public Set<Pattern> getInSet() {
		return inSet;
	}
	
	public Set<Pattern> getOutSet() {
		return outSet;
	}
	
    public int getType() {
        return type;
    }

    public String getName() {
    	return name;
    }
    
    public void setTag(int tag) {
    	this.tag = tag;
    }
    
    public int getTag() {
    	return tag;
    }
}
