package com.sinan.dpviewer.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;

import com.sinan.dpviewer.util.Logger;

public class PatternProcessor {
	
	private static Collection<Clazz> classList;
	private static int edgeCount;
	private static Collection<Pattern> patterns;
	private static HashMap<Integer, Clazz> classDict;	
	private IProgressMonitor monitor;
	
	public PatternProcessor(Collection<Clazz> cl, int ec, IProgressMonitor monitor) {
		classList = cl;
		edgeCount = ec;
		classDict = new HashMap<Integer, Clazz>();
		this.monitor = monitor;
	}

	public void process() throws InterruptedException {
		monitor.subTask("Finding cycles");
		CycleClassFinder cycleClassFinder = new CycleClassFinder(classList);
		cycleClassFinder.find();
		monitor.worked(3333);

		monitor.subTask("Finding authorities");
		AuthorityClassFinder authorityClassFinder = new AuthorityClassFinder(
				classList, edgeCount,
				AuthorityClassFinder.DEFAULT_DEPENDENCY_PERCENTAGE);
		authorityClassFinder.find();
		monitor.worked(3333);		

		monitor.subTask("Finding hubs");		
		HubClassFinder hubClassFinder = new HubClassFinder(classList,
				edgeCount, HubClassFinder.DEFAULT_DEPENDENCY_PERCENTAGE);
		hubClassFinder.find();
		monitor.worked(3334);		
		
		monitor.subTask("Finding bridges");	
		// find bridges
		int[][] graph = convertClassListToMatrix(classList);
		MyClust bridgeFinder = new MyClust(graph, classList.size(), edgeCount, "1", "2", "1", monitor);
		Map<Integer[], Integer[]> bridgeMap = bridgeFinder.getBridgeNodes();
		Set<Integer[]> keysToRemove = new HashSet<Integer[]>();
        for (Integer[] set : bridgeMap.keySet()) {
            if (set.length <= 2 || bridgeMap.get(set).length <= 2) {
            	keysToRemove.add(set);
            }
        }
        for (Integer[] set : keysToRemove) {
        	bridgeMap.remove(set);
        }
		monitor.worked(10000);	               
        
		Set<Pattern> bridgePatterns = new HashSet<Pattern>();
		Logger.log("Bridges: " + bridgeMap);		
        
		int j = 1;
		for (Integer[] set : bridgeMap.keySet()) {
        	Integer[] clzTags1 = set;
        	Integer[] clzTags2 = bridgeMap.get(set);        	
        	Set<Clazz> clzes = new HashSet<Clazz>();
        	for (Integer i : clzTags1) {
        		Clazz c = classDict.get(i);
        		clzes.add(c); 		     		
        	}
        	for (Integer i : clzTags2) {
        		Clazz c = classDict.get(i);
        		clzes.add(c);
            	     		      		
        	}     
        	
        	// remove bridges from list, so that they won't be clustered again   
        	classList.remove(clzes);
        	
        	Pattern p = new Pattern("B" + j, Clazz.BRIDGE, clzes);
        	bridgePatterns.add(p);
        	j++;
        }

		monitor.subTask("Finding islands");	
		IslandFinder islandFinder = new IslandFinder(classList, edgeCount);
		patterns = islandFinder.cluster();
		patterns.addAll(bridgePatterns);
		
		// add connections
		for (Pattern p1 : patterns) {

			Collection<Clazz> couts = new HashSet<Clazz>();
			if (p1.getType() != Clazz.ISLAND 
					&& p1.getType() != Clazz.BRIDGE) {
				couts = p1.getClazz().getOutList();
			} else {
				for (Clazz c : p1.getClazzes()) {
					couts.addAll(c.getOutList());
				}
			}

			for (Pattern p2 : patterns) {

				Collection<Clazz> cs = new HashSet<Clazz>();
				if (p2.getType() != Clazz.ISLAND
						&& p2.getType() != Clazz.BRIDGE) {
					cs.add(p2.getClazz());
				} else {
					cs.addAll(p2.getClazzes());
				}
				
				for (Clazz c : cs) {
					if (couts.contains(c)) {
						p1.getOutSet().add(p2);
						p2.getInSet().add(p1);
						break;
					}
				}
			}
		}
		monitor.worked(10000);	
	}
	
	public int[][] convertClassListToMatrix(Collection<Clazz> classList) {
		int nodes = classList.size();
		int[][] graph = new int[nodes][nodes];
		
		for (int i = 0; i < nodes; i++) {
			for (int j = 0; j < nodes; j++) {
				graph[i][j] = 0;
			}
		}
		
		classDict = new HashMap<Integer, Clazz>();
		
		ArrayList<Clazz> lst = new ArrayList<Clazz>(classList);
		Collections.sort(lst, new Comparator<Clazz>() {

			@Override
			public int compare(Clazz arg0, Clazz arg1) {
				if (arg0.getTag() > arg1.getTag())
					return 1;
				if (arg0.getTag() < arg1.getTag())
					return -1;
				return 0;
			}
		});
        
		for (int i = 0; i < lst.size(); i++) {
			Clazz klass = lst.get(i);
			classDict.put(klass.getTag(), klass);
		}  
		
		for (int i = 0; i < lst.size(); i++) {
			Clazz klass = lst.get(i);
			for (Clazz c : klass.getOutList()) {
				graph[i][c.getTag()] = 1;
			}
		}
		
		return graph;
	}

	public static Collection<Clazz> getClassList() {
		return classList;
	}

	public static Collection<Pattern> getPatterns() {
		return patterns;
	}
}
