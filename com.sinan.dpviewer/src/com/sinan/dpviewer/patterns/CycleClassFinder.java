/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sinan.dpviewer.patterns;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sinan
 */
public class CycleClassFinder implements DependencyPatternClassFinder {

    private Collection<Clazz> classList;
    private Collection<Clazz> cycleClassList;

    public CycleClassFinder(Collection<Clazz> classList) {
        this.classList = classList;
        this.cycleClassList = new ArrayList<Clazz>();
    }

    @Override
    public Collection<Clazz> find() {

        cycleClassList.clear();
        for (Clazz klass : classList) {

            if (klass.getOutList().contains(klass)) {
                klass.setType(Clazz.CYCLE);
                cycleClassList.add(klass);
            }
        }

        return cycleClassList;
    }

}
