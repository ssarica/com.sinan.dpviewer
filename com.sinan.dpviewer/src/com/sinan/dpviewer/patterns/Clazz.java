package com.sinan.dpviewer.patterns;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sinan.dpviewer.metrics.MetricContext;

/**
 *
 * @author sinan
 */
public class Clazz {

    public static final int NONCLASSIFIED = 0;
    // Single class dependency patterns
    public static final int AUTHORITY = 1;
    public static final int HUB = 2;
    public static final int CYCLE = 3;
    // Multi class dependency patterns
    public static final int ISLAND = 4;
    public static final int BRIDGE = 5;
    // This one is a defect type
    public static final int GOD = 6;

    private String name;
    private String fullyQualifiedName;
    private String path;    
    private int lineNumber;
    
    private Set<Clazz> inList;
    private Set<Clazz> outList;
    private int type = 0;
    private int tag = -99999;
    private int hiddenTag = -99999;
    
    private MetricContext metrics;
    
    public Clazz(String name) {
        this.name = name;
        inList = new HashSet<Clazz>();
        outList = new HashSet<Clazz>();
        metrics = new MetricContext();
    }    

    public Clazz(String name, String fullyQualifiedName, String path, int lineNumber) {
        this.name = name;
    	this.fullyQualifiedName = fullyQualifiedName;
    	this.path = path;
    	this.lineNumber = lineNumber;
        inList = new HashSet<Clazz>();
        outList = new HashSet<Clazz>();
        metrics = new MetricContext();
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    /**
     * @return the fullyQualifiedName
     */
    public String getFullyQualifiedName() {
        return fullyQualifiedName;
    }    
    
    /**
     * @param fullyQualifiedName the fullyQualifiedName to set
     */
    public String setFullyQualifiedName(String fullyQualifiedName) {
        return fullyQualifiedName;
    }        
    
	/**
	 * @param the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the path
	 */
	public int getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber
	 *            the lineNumber to set
	 */
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}    

    /**
     * @return the inList
     */
    public Set<Clazz> getInList() {
        return inList;
    }

    /**
     * @return the outList
     */
    public Set<Clazz> getOutList() {
        return outList;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Clazz) {
            if (this.name == null ? ((Clazz) obj).getName() == null
                    : this.name.equals(((Clazz) obj).getName())) {
                return true;
            }
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }
    
    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the tag
     */
    public int getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(int tag) {
        this.tag = tag;
    }

    /**
     * @return the hiddenTag
     */
    public int getHiddenTag() {
        return hiddenTag;
    }

    /**
     * @param hiddenTag the hiddenTag to set
     */
    public void setHiddenTag(int hiddenTag) {
        this.hiddenTag = hiddenTag;
    }

    /**
     * @return the metrics
     */
	public MetricContext getMetrics() {
		return metrics;
	}

    /**
     * @param metrics the metrics to set
     */
	public void setMetrics(MetricContext metrics) {
		this.metrics = metrics;
	}
    
}
