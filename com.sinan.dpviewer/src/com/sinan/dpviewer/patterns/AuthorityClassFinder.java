/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sinan.dpviewer.patterns;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sinan
 */
public class AuthorityClassFinder implements DependencyPatternClassFinder {
	
	public static float DEFAULT_DEPENDENCY_PERCENTAGE = 0.04f;

    private Collection<Clazz> classList;
    private Collection<Clazz> authorityClassList;
    private int edgeCount;
    private float dependencyPercentage;

    public AuthorityClassFinder(Collection<Clazz> classList, int edgeCount, float dependencyPercentage) {
        this.classList = classList;
        this.edgeCount = edgeCount;
        this.dependencyPercentage = dependencyPercentage;
        this.authorityClassList = new ArrayList<Clazz>();
    }

    @Override
    public Collection<Clazz> find() {

        authorityClassList.clear();
        for (Clazz klass : classList) {
            float depPer = (float)klass.getInList().size() / edgeCount;
            if (depPer > dependencyPercentage) {
                klass.setType(Clazz.AUTHORITY);
                authorityClassList.add(klass);
            }
        }

        return authorityClassList;
    }

}
