package com.sinan.dpviewer.util;

public class Logger {
	
	public static void log(String message) {
		System.out.println(message);
	}

	public static void log(Exception ex) {
		System.out.println(ex);
	}
	
	public static void log(Object obj) {
		System.out.println(obj);
	}	

}
