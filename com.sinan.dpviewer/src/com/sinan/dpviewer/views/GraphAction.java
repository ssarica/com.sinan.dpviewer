package com.sinan.dpviewer.views;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.zest.core.viewers.GraphViewer;

public class GraphAction extends Action {
	protected final GraphViewer viewer;

	/**
	 * Get the viewer
	 * 
	 * @return
	 */
	protected GraphViewer getViewer() {
		return viewer;
	}

	/**
	 * Get the current selection of the viewer
	 * 
	 * @return the current selection
	 */
	protected IStructuredSelection getSelection() {
		return (IStructuredSelection) viewer.getSelection();
	}

	/**
	 * Creates an action for the given GraphViewer
	 * 
	 * @param viewer
	 */
	public GraphAction(GraphViewer viewer) {
		this.viewer = viewer;
	}
}
