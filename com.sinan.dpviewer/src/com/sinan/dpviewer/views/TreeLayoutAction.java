package com.sinan.dpviewer.views;

import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;

public class TreeLayoutAction extends GraphAction {

	public TreeLayoutAction(GraphViewer viewer) {
		super(viewer);
		setText("Tree layout");
		setToolTipText("Re-applies tree graph layout");
	}

	@Override
	public void run() {
		viewer.setLayoutAlgorithm(new TreeLayoutAlgorithm(
				LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
	}
}
