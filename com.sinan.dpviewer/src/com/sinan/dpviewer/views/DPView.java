package com.sinan.dpviewer.views;

import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;

import com.sinan.dpviewer.astparser.ASTProcessor;
import com.sinan.dpviewer.patterns.Clazz;
import com.sinan.dpviewer.patterns.Pattern;
import com.sinan.dpviewer.patterns.PatternProcessor;
import com.sinan.dpviewer.util.Logger;

public class DPView extends ViewPart {

	public static final String ID = "com.sinan.dpviewer.views.dpview";
	private GraphViewer graphViewer;
	private HashMap<String, GraphNode> nodes = new HashMap<String, GraphNode>();

	private IAction radialLayoutAction;
	private IAction springLayoutAction;
	private IAction treeLayoutAction;
	private IAction gotoSourceCodeAction;
	private IAction showClassesAction;
	private IAction backtoPatternsAction;
	private IAction openMetricsViewAction;

	private final ReentrantLock lock = new ReentrantLock();
	private boolean drawPatternClazzesMode = false;
	
	public static Clazz selectedClazz;

	public void createPartControl(Composite parent) {
		Logger.log("DPView.createPartControl() called.");

		// Graph will hold all other objects
		graphViewer = new GraphViewer(parent, SWT.NONE);
		this.drawGraph();

		// create right-click menu
		final MenuManager mm = new MenuManager();
		graphViewer.getGraphControl().setMenu(
				mm.createContextMenu(graphViewer.getGraphControl()));
		gotoSourceCodeAction = new GotoSourceCodeAction(graphViewer);
		radialLayoutAction = new RadialLayoutAction(graphViewer);
		springLayoutAction = new SpringLayoutAction(graphViewer);
		treeLayoutAction = new TreeLayoutAction(graphViewer);
		showClassesAction = new ShowClassesAction(graphViewer, this);
		backtoPatternsAction = new BackToPatternsAction(graphViewer, this);
		openMetricsViewAction = new OpenMetricsViewAction(graphViewer);
		
		mm.add(new Action("never shown entry") {}); // needed if it's a submenu
		mm.setRemoveAllWhenShown(true);
		mm.addMenuListener(new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {
				Object obj = ((IStructuredSelection) graphViewer.getSelection()).getFirstElement();
				if (obj instanceof Clazz) {
					if (drawPatternClazzesMode) {
						mm.add(gotoSourceCodeAction);				
						mm.add(openMetricsViewAction);
						mm.add(new Separator());
						mm.add(backtoPatternsAction);						
					} else {		
						mm.add(gotoSourceCodeAction);
						mm.add(openMetricsViewAction);
					}
				} else if (obj instanceof Pattern) {
					Pattern p = (Pattern) obj;
					if (p.getClazz() != null && p.getClazz().getPath() != null) {
						mm.add(gotoSourceCodeAction);
						mm.add(openMetricsViewAction);
						mm.add(new Separator());					
					} else if (p.getType() == Clazz.ISLAND
							|| p.getType() == Clazz.BRIDGE) {
						mm.add(showClassesAction);
						mm.add(new Separator());
					}
					mm.add(radialLayoutAction);
					mm.add(treeLayoutAction);
				} else {
					if (drawPatternClazzesMode) {
						mm.add(backtoPatternsAction);
						mm.add(new Separator());
					}					
					mm.add(radialLayoutAction);
					mm.add(treeLayoutAction);
				}
			}
		});
		getSite().registerContextMenu(mm, graphViewer);
		
		graphViewer.getGraphControl().addSelectionListener(
				new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						// Logger.log(e);
						Clazz c = null;
						Object obj = e.item.getData();
						if (obj instanceof Pattern) {
							Pattern p = (Pattern) obj;
							c = p.getClazz();
						} else if (obj instanceof Clazz) {
							c = (Clazz) obj;
						}
						selectedClazz = c;
					}
				});

		// workbench te baska bir yere tiklandiginda
		// getViewSite().getPage().addSelectionListener(new ISelectionListener()
		// {
		//
		// @Override
		// public void selectionChanged(IWorkbenchPart part, ISelection
		// selection) {
		// // TODO Auto-generated method stub
		// Logger.log("DPView.ISelectionListener.selectionChanged() called.");
		// }
		// });
	}

	public void drawGraph() {
		if (lock.tryLock()) {
			try {
				drawPatternClazzesMode = false;

				if (ASTProcessor.getClassList() != null
						&& !ASTProcessor.getClassList().isEmpty()) {
					// refresh graph
					clearGraph(graphViewer.getGraphControl());
					graphViewer.getGraphControl().redraw();
					nodes.clear();

					for (Pattern p : PatternProcessor.getPatterns()) {
						Logger.log("drawing pattern node "
								+ p.getName());
						GraphNode node = null;
						
						String packInfo = "";
						if (p.getClazz() != null) {
							packInfo = p.getClazz().getPath() == null ? " - source code not available" : "";
						}

						switch (p.getType()) {
						case Clazz.AUTHORITY:
							node = new SingleClassPatternNode(
									graphViewer.getGraphControl(), SWT.NONE,
									p.getName(), p);								
							node.setBackgroundColor(new Color(graphViewer.getControl().getDisplay(), 255, 148, 112));
							node.setText("A");
							((SingleClassPatternNode) node).setTooltipText(p.getClazz().getName() + packInfo);
							break;
						case Clazz.HUB:
							node = new SingleClassPatternNode(
									graphViewer.getGraphControl(), SWT.NONE,
									p.getName(), p);								
							node.setBackgroundColor(new Color(graphViewer.getControl().getDisplay(), 255, 153, 255));
							node.setText("H");
							((SingleClassPatternNode) node).setTooltipText(p.getClazz().getName() + packInfo);
							break;
						case Clazz.CYCLE:
							node = new SingleClassPatternNode(
									graphViewer.getGraphControl(), SWT.NONE,
									p.getName(), p);								
							node.setBackgroundColor(new Color(graphViewer.getControl().getDisplay(), 112, 219, 255));
							node.setText("C");
							((SingleClassPatternNode) node).setTooltipText(p.getClazz().getName() + packInfo);
							break;	
						case Clazz.ISLAND:
							node = new IslandNode(
									graphViewer.getGraphControl(), SWT.NONE,
									p.getName(), p);								
							node.setBackgroundColor(new Color(graphViewer.getControl().getDisplay(), 51, 255, 204));
							node.setText(node.getText());
							float multiplier = (float) p.getClazzes().size() / 6;
							if (multiplier > 4f)
								multiplier = 4f;
							node.setSize(node.getSize().width * multiplier, node.getSize().height * multiplier);
							break;
						case Clazz.BRIDGE:
							node = new BridgeNode(
									graphViewer.getGraphControl(), SWT.NONE,
									p.getName(), p);								
							node.setBackgroundColor(new Color(graphViewer.getControl().getDisplay(), 153, 153, 255));
							node.setText(node.getText());
							float mult = (float) p.getClazzes().size() / 3;
							if (mult > 2.5f)
								mult = 2.5f;
							node.setSize(node.getSize().width * mult, node.getSize().height * mult);
							break;									
						default:
							break;
						}
						
						nodes.put(p.getName(), node);
					}

					for (Pattern p1 : PatternProcessor.getPatterns()) {
						for (Pattern p2 : p1.getOutSet()) {
							Logger.log("Drawing connection from "
									+ p1.getName() + " to " + p2.getName());
							GraphNode n1 = nodes.get(p1.getName());
							GraphNode n2 = nodes.get(p2.getName());
							new GraphConnection(
									graphViewer.getGraphControl(),
									ZestStyles.CONNECTIONS_SOLID, n1, n2);
						}
					}

					graphViewer.getGraphControl().setLayoutAlgorithm(
							new RadialLayoutAlgorithm(
									LayoutStyles.NO_LAYOUT_NODE_RESIZING),
							true);
				}
			} finally {
				lock.unlock();
			}
		}
	}
	
	public void drawPatternClasses(Pattern p) {
		drawPatternClazzesMode = true;
		clearGraph(graphViewer.getGraphControl());
		graphViewer.getGraphControl().redraw();
		HashMap<String, GraphNode> clazzNodes = new HashMap<String, GraphNode>();		

		for (Clazz c : p.getClazzes()) {
			Logger.log("drawing clazz node " + c.getName());
			GraphNode node = new GraphNode(graphViewer.getGraphControl(),
					SWT.NONE, c.getName(), c);
			clazzNodes.put(c.getName(), node);
		}

		for (Clazz c1 : p.getClazzes()) {
			for (Clazz c2 : c1.getOutList()) {
				Logger.log("Drawing connection from " + c1.getName() + " to "
						+ c2.getName());
				GraphNode n1 = clazzNodes.get(c1.getName());
				GraphNode n2 = clazzNodes.get(c2.getName());
				if (n1 != null && n2 != null) {
					new GraphConnection(graphViewer.getGraphControl(),
							ZestStyles.CONNECTIONS_DIRECTED, n1, n2);
				}
			}
		}

		graphViewer.getGraphControl().setLayoutAlgorithm(
				new RadialLayoutAlgorithm(
						LayoutStyles.NO_LAYOUT_NODE_RESIZING),
				true);
	}

	private void clearGraph(Graph g) {
		// remove all the connections
		Object[] objects = g.getConnections().toArray();
		for (int x = 0; x < objects.length; x++)
			((GraphConnection) objects[x]).dispose();

		// remove all the nodes
		objects = g.getNodes().toArray();
		for (int x = 0; x < objects.length; x++)
			((GraphNode) objects[x]).dispose();
	}

	public void setFocus() {
		Logger.log("DPView.setFocus() called.");
		this.drawGraph();
	}
}