package com.sinan.dpviewer.views;

import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;

public class RadialLayoutAction extends GraphAction {

	public RadialLayoutAction(GraphViewer viewer) {
		super(viewer);
		setText("Radial layout");
		setToolTipText("Re-applies radial graph layout");
	}

	@Override
	public void run() {
		viewer.setLayoutAlgorithm(new RadialLayoutAlgorithm(
				LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
	}
}