package com.sinan.dpviewer.views;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.zest.core.viewers.GraphViewer;

import com.sinan.dpviewer.patterns.Clazz;
import com.sinan.dpviewer.patterns.Pattern;
import com.sinan.dpviewer.util.Logger;

public class GotoSourceCodeAction extends GraphAction {

	public GotoSourceCodeAction(GraphViewer viewer) {
		super(viewer);
		setText("Open source code");
		setToolTipText("Opens the source code of selected node");
	}

	@Override
	public void run() {
		Clazz c = null;
		Object obj = getSelection().getFirstElement();
		if (obj instanceof Clazz) {
			c = (Clazz) obj;
		} else if (obj instanceof Pattern) {
			c = ((Pattern) obj).getClazz();
		}
		
		Logger.log("GotoSourceCodeAction " + c);
		if (c != null) {
			Logger.log("source : " + c.getPath()
					+ ", line number: " + c.getLineNumber());
			
			Logger.log("metrics: NumAttr: " + c.getMetrics().NumAttr
					+ " NumOps: " + c.getMetrics().NumOps
					+ " NumPubOps: " + c.getMetrics().NumPubOps
					+ " Getters: " + c.getMetrics().Getters
					+ " Setters: " + c.getMetrics().Setters
					+ " Dep_In: " + c.getMetrics().Dep_In
					+ " Dep_Out: " + c.getMetrics().Dep_Out
					+ " EC_Attr: " + c.getMetrics().EC_Attr
					+ " EC_Par: " + c.getMetrics().EC_Par
					+ " IC_Attr: " + c.getMetrics().IC_Attr
					+ " IC_Par: " + c.getMetrics().IC_Par);

			if (c.getPath() != null) {
				// open file in ide
				IWorkbenchPage page = PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage();
				try {
					IEditorPart editorPart = IDE.openEditor(
							page,
							ResourcesPlugin
									.getWorkspace()
									.getRoot()
									.getFile(
											Path.fromPortableString(c
													.getPath())));
					if ((editorPart instanceof ITextEditor)
							|| c.getLineNumber() <= 0) {
						ITextEditor editor = (ITextEditor) editorPart;
						IDocument document = editor.getDocumentProvider()
								.getDocument(editor.getEditorInput());
						if (document != null) {
							IRegion lineInfo = null;
							try {
								// line count internaly starts with 0, and
								// not with 1 like in
								// GUI
								lineInfo = document.getLineInformation(c
										.getLineNumber());
							} catch (BadLocationException ex) {
								// ignored because line number may not
								// really exist in document,
								// we guess this...
							}
							if (lineInfo != null) {
								editor.selectAndReveal(
										lineInfo.getOffset(),
										lineInfo.getLength());
							}
						}
					}
				} catch (PartInitException ex) {
					// Put your exception handler here if you wish to
				}
			}
		}
	}
}
