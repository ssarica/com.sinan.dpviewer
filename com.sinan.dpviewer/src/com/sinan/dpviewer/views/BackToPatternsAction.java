package com.sinan.dpviewer.views;

import org.eclipse.zest.core.viewers.GraphViewer;

public class BackToPatternsAction extends GraphAction {
	
	private DPView dpview;

	public BackToPatternsAction(GraphViewer viewer, DPView view) {
		super(viewer);
		setText("Back");
		setToolTipText("Back to dependency patterns view");
		dpview = view;
	}

	@Override
	public void run() {
		dpview.drawGraph();
	}
}
