package com.sinan.dpviewer.views;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.IContainer;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.core.widgets.internal.GraphLabel;

public class SingleClassPatternNode extends GraphNode {
	
	private String tooltipText; 

	public SingleClassPatternNode(IContainer graphModel, int style, String text, Object data) {
		super(graphModel, style, text, data);
		// TODO Auto-generated constructor stub
	}
	
	protected void updateFigureForModel(IFigure currentFigure) {
		if (currentFigure == null) {
			return;
		}

		if (!(currentFigure instanceof SingleClassPatternLabel)) {
			return;
		}
		SingleClassPatternLabel figure = (SingleClassPatternLabel) currentFigure;
		IFigure toolTip;

		if (!checkStyle(ZestStyles.NODES_HIDE_TEXT)) {
			figure.setText(this.getText());
		}
		figure.setIcon(getImage());

		if (highlighted == HIGHLIGHT_ON) {
			figure.setForegroundColor(getForegroundColor());
			figure.setBackgroundColor(getHighlightColor());
			figure.setBorderColor(getBorderHighlightColor());
		} else {
			figure.setForegroundColor(getForegroundColor());
			figure.setBackgroundColor(getBackgroundColor());
			figure.setBorderColor(getBorderColor());
		}

		figure.setBorderWidth(getBorderWidth());

		figure.setFont(getFont());

		if (this.getTooltip() == null) {
			// if we have a custom tooltip, don't try and create our own.
			toolTip = new Label();
			((Label) toolTip).setText(tooltipText);
		} else {
			toolTip = this.getTooltip();
			((Label) toolTip).setText(tooltipText);
		}
		figure.setToolTip(toolTip);

		refreshLocation();
	}	
	
	public void setTooltipText(String text) {
		tooltipText = text;
	}
	
	@Override
	protected IFigure createFigureForModel() {
		SingleClassPatternNode node = this;
		boolean cacheLabel = (this).cacheLabel();
		SingleClassPatternLabel label = new SingleClassPatternLabel(node.getText(), node.getImage(), cacheLabel);
		label.setFont(this.getFont());
		if (checkStyle(ZestStyles.NODES_HIDE_TEXT)) {
			label.setText("");
		}
		updateFigureForModel(label);
		return label;
	}

}
