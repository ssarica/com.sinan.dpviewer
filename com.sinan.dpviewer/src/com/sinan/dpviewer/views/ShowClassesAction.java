package com.sinan.dpviewer.views;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.zest.core.viewers.GraphViewer;

import com.sinan.dpviewer.patterns.Pattern;

public class ShowClassesAction extends GraphAction {
	
	private DPView dpview;

	public ShowClassesAction(GraphViewer viewer, DPView view) {
		super(viewer);
		setText("Show classes");
		setToolTipText("Show classes in this pattern");
		dpview = view;
	}

	@Override
	public void run() {
		Object obj = ((IStructuredSelection) viewer.getSelection()).getFirstElement();
		if (obj instanceof Pattern) {
			Pattern p = (Pattern) obj;
			dpview.drawPatternClasses(p);
		}
	}
}