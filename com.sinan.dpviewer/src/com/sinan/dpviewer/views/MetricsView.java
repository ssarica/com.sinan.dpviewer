package com.sinan.dpviewer.views;


import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;

import com.sinan.dpviewer.metrics.MetricContext;
import com.sinan.dpviewer.util.Logger;


public class MetricsView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "com.sinan.dpviewer.views.metricsview";

	private TableViewer viewer;
	private MetricContext metrics;
	
	private MetricData[] metricData;
	
	class MetricData {
		
		public String name;
		public int value;		
		
		public MetricData(String name, int value) {
			this.name = name;
			this.value = value;
		}	
	}
	
	class ViewContentProvider implements IStructuredContentProvider {
		
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		
		public void dispose() {
		}
		
		public Object[] getElements(Object parent) {
			if (metrics != null) {
				return metricData;
			} else {
				return new String[] {};
			}
		}
	}
	
	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		
		public String getColumnText(Object obj, int index) {
			if (obj instanceof MetricData) {
				MetricData md = (MetricData) obj;
				switch (index) {
				case 0:
					return md.name;

				case 1:
					return String.valueOf(md.value);
					
				default:
					break;
				}
			}
			return "";
		}
		
		public Image getColumnImage(Object obj, int index) {
			return null;
		}
	}
	
	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public MetricsView() {
	}

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setInput(getViewSite());
		
		Table table = viewer.getTable();
	    table.setLayoutData(new GridData(GridData.FILL_BOTH));
	    
	    TableColumn tc = new TableColumn(table, SWT.LEFT);
	    tc.setText("Metric");
	    
	    tc = new TableColumn(table, SWT.LEFT);
	    tc.setText("Value");
	    
	    // Pack the columns
	    for (int i = 0, n = table.getColumnCount(); i < n; i++) {
	      table.getColumn(i).pack();
	    }

	    // Turn on the header and the lines
	    table.setHeaderVisible(true);
	    table.setLinesVisible(true);
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		if (DPView.selectedClazz != null) {
			metrics = DPView.selectedClazz.getMetrics();
			
			metricData = new MetricData[11];
			metricData[0] = new MetricData("NumAttr", metrics.NumAttr);
			metricData[1] = new MetricData("NumOps", metrics.NumOps);
			metricData[2] = new MetricData("NumPubOps", metrics.NumPubOps);
			metricData[3] = new MetricData("Getters", metrics.Getters);
			metricData[4] = new MetricData("Setters", metrics.Setters);
			metricData[5] = new MetricData("Dep_In", metrics.Dep_In);
			metricData[6] = new MetricData("Dep_Out", metrics.Dep_Out);
			metricData[7] = new MetricData("EC_Attr", metrics.EC_Attr);
			metricData[8] = new MetricData("EC_Par", metrics.EC_Par);
			metricData[9] = new MetricData("IC_Attr", metrics.IC_Attr);	
			metricData[10] = new MetricData("IC_Par", metrics.IC_Par);			
			
			viewer.refresh();
			
			// Pack the columns
			for (int i = 0, n = viewer.getTable().getColumnCount(); i < n; i++) {
				viewer.getTable().getColumn(i).pack();
			}		
			
			Logger.log("metrics: NumAttr: " + metrics.NumAttr
					+ " NumOps: " + metrics.NumOps
					+ " NumPubOps: " + metrics.NumPubOps
					+ " Getters: " + metrics.Getters
					+ " Setters: " + metrics.Setters
					+ " Dep_In: " + metrics.Dep_In
					+ " Dep_Out: " + metrics.Dep_Out
					+ " EC_Attr: " + metrics.EC_Attr
					+ " EC_Par: " + metrics.EC_Par
					+ " IC_Attr: " + metrics.IC_Attr
					+ " IC_Par: " + metrics.IC_Par);	
		}
		
		viewer.getControl().setFocus();
	}
}