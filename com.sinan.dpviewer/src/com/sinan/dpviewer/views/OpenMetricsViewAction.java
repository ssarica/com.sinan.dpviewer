package com.sinan.dpviewer.views;

import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.zest.core.viewers.GraphViewer;

public class OpenMetricsViewAction extends GraphAction {

	public OpenMetricsViewAction(GraphViewer viewer) {
		super(viewer);
		setText("Show metrics");
		setToolTipText("Opens a view showing class OOP metrics");
	}

	@Override
	public void run() {
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("com.sinan.dpviewer.views.metricsview");
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}