package com.sinan.dpviewer.views;

import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;

public class SpringLayoutAction extends GraphAction {

	public SpringLayoutAction(GraphViewer viewer) {
		super(viewer);
		setText("Spring layout");
		setToolTipText("Re-applies spring graph layout");
	}

	@Override
	public void run() {
		viewer.setLayoutAlgorithm(new SpringLayoutAlgorithm(
				LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
	}
}