package com.sinan.dpviewer.astparser;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ArrayType;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import com.sinan.dpviewer.metrics.MetricContext;
import com.sinan.dpviewer.patterns.Clazz;
import com.sinan.dpviewer.util.Logger;

public class ASTProcessor {

	private static IJavaProject currentProject = null;

	private static ConcurrentHashMap<String, Clazz> classList = null;
	private static ConcurrentHashMap<String, Clazz> previousClassList = null;
	private static int edgeCount;
	private static int tag;
	private static boolean classListChangedLastRun = false;

	private IProgressMonitor monitor;
	private final ReentrantLock lock = new ReentrantLock();

	private CompilationUnit currentCompilationUnit;
	private ICompilationUnit currentFile;
	
	private static ConcurrentHashMap<String, MetricContext> metricsMap = null;
 
	/**
	 * 
	 * @param project
	 * @param monitor 
	 */
	public ASTProcessor(IJavaProject project, IProgressMonitor monitor) {
		currentProject = project;
		this.monitor = monitor;
	}

	/**
	 * 
	 */
	public ASTProcessor() {
	}

	/**
	 * @return true if AST has changed
	 * @throws InterruptedException 
	 */
	public boolean process() throws InterruptedException {
		if (currentProject == null)
			return false;

		if (lock.tryLock()) {
			classList = new ConcurrentHashMap<String, Clazz>();
			metricsMap = new ConcurrentHashMap<String, MetricContext>();
			edgeCount = 0;
			tag = 0;
			IPackageFragment[] packages;
			try {
				// Calculate total number compilation units to process
				int totalWork = 0;
				packages = currentProject.getPackageFragments();
				for (IPackageFragment mypackage : packages) {
					if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
						totalWork += mypackage.getCompilationUnits().length;
					}
				}
				monitor.subTask("Building and processing AST tree");
				packages = currentProject.getPackageFragments();
				for (IPackageFragment mypackage : packages) {
					if (mypackage.getKind() == IPackageFragmentRoot.K_SOURCE) {
						for (ICompilationUnit unit : mypackage
								.getCompilationUnits()) {
							currentFile = unit;
							// Now create the AST for the ICompilationUnits
							ASTParser parser = ASTParser.newParser(AST.JLS3);
							parser.setKind(ASTParser.K_COMPILATION_UNIT);
							parser.setSource(currentFile);
							parser.setResolveBindings(true);
							currentCompilationUnit = (CompilationUnit) parser
									.createAST(null);
							currentCompilationUnit.accept(new DPTypeVisitor());
							currentCompilationUnit
									.accept(new DPConnectionVisitor());
							monitor.worked(50000 / totalWork);
							if (monitor.isCanceled()) {
			                    monitor.done();
			                    throw new InterruptedException();
							}
						}
					}
				}
				if (classList.equals(previousClassList)) {
					classListChangedLastRun = false;
					return false;
				} else {
					classListChangedLastRun = true;
					previousClassList = classList;
					return true;
				}
			} catch (JavaModelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}

		return false;
	}

	/**
	 * 
	 */
	public static boolean hasClassListChangedLastRun() {
		if (classListChangedLastRun) {
			classListChangedLastRun = false;
			return true;
		}
		return classListChangedLastRun;
	}

	/**
	 * 
	 * @return
	 */
	public static Collection<Clazz> getClassList() {
		if (classList == null)
			return null;
		return classList.values();
	}
	
	/**
	 * 
	 * @return
	 */
	public static int getEdgeCount() {
		return edgeCount;
	}	

	/**
	 * 
	 * @author saricas
	 * 
	 */
	public class DPTypeVisitor extends ASTVisitor {

		@Override
		public boolean visit(TypeDeclaration node) {

			String packageName = "";
			if (currentCompilationUnit.getPackage() != null)
				packageName = currentCompilationUnit.getPackage().getName()
						.getFullyQualifiedName();

			String fullyQualifiedName;
			if (packageName == "")
				fullyQualifiedName = node.getName().getFullyQualifiedName();
			else
				fullyQualifiedName = packageName + "."
						+ node.getName().getFullyQualifiedName();

			String name = node.getName().getIdentifier();
			String path = currentFile.getPath().toPortableString();
			int lineNumber = currentCompilationUnit.getLineNumber(node
					.getStartPosition()) - 1;

			Logger.log("MyVisitor - CompilationUnit - "
					+ fullyQualifiedName + " LineNumber " + lineNumber
					+ " in source " + path);

			Clazz c = new Clazz(name, fullyQualifiedName, path, lineNumber);
			if (!classList.contains(c)) {
				c.setTag(tag++);
				classList.put(name, c);
			} else {
				// bazen DPConnectionVisitor daha once visit edebiliyor tipleri
				// bu durumda compilation unit'den once clazz hashset'e konmus
				// oluyor. fakat kaynak kodun nerede oldugu bilgisi ancak 
				// compilation unit'de DTypeVisitor ile alinabilir.
				classList.get(name).setPath(path);
				classList.get(name).setLineNumber(lineNumber);
				classList.get(name).setFullyQualifiedName(fullyQualifiedName);
			}
			return super.visit(node);
		}
	}

	private String getPackageName(TypeDeclaration node) {
		String packageName = "";
		if (currentCompilationUnit.getPackage() != null)
			packageName = currentCompilationUnit.getPackage().getName()
					.getFullyQualifiedName();
		return packageName;
	}

	private String getFullyQualifiedName(TypeDeclaration node) {
		String packageName = getPackageName(node);
		String fullyQualifiedName;
		if (packageName == "")
			fullyQualifiedName = node.getName().getFullyQualifiedName();
		else
			fullyQualifiedName = packageName + "."
					+ node.getName().getFullyQualifiedName();
		return fullyQualifiedName;
	}

	/**
	 * 
	 * @author saricas
	 * 
	 */
	public class DPConnectionVisitor extends ASTVisitor {
		
		private void addConnection(Type type, ASTNode node, String declaration) {
			if (!type.isPrimitiveType()) {
				
				// if String[][], make it String
				while (type.isArrayType()) {
					type = ((ArrayType) type).getComponentType();
				}
				
				Clazz c1 = null;
				if (classList.containsKey(type.toString())) {
					c1 = classList.get(type.toString());
				} else {
					c1 = new Clazz(type.toString());
					c1.setTag(tag++);
					classList.put(type.toString(), c1);
				}

				Clazz c2 = null;
				TypeDeclaration td = (TypeDeclaration) node.getParent();
				if (classList.containsKey(td.getName().getIdentifier())) {
					c2 = classList.get(td.getName().getIdentifier());
				} else {
					c2 = new Clazz(td.getName().getIdentifier());
					c2.setTag(tag++);
					classList.put(td.getName().getIdentifier(), c2);
				}

				Logger.log("DPConnectionVisitor - " + c2.getName()
						+ " has a dependency to " + c1.getName());
				if (!c1.getInList().contains(c2)) {
					c1.getInList().add(c2);
					c2.getOutList().add(c1);
					edgeCount++;
					c1.getMetrics().Dep_In++;
					c2.getMetrics().Dep_Out++;
					if (declaration == "FieldDeclaration") {
						c1.getMetrics().EC_Attr++;
						c2.getMetrics().IC_Attr++;
					} else if (declaration == "MethodDeclaration") {
						c1.getMetrics().EC_Par++;
						c2.getMetrics().IC_Par++;
					}
				}
				Logger.log("DPConnectionVisitor - " + c1.getInList());
				Logger.log("DPConnectionVisitor - " + c2.getOutList());
			}
		}
		
		private void addConnection(ITypeBinding type, ASTNode node) {
			if (!type.isPrimitive()) {
				
				// if String[][], make it String
				while (type.isArray()) {
					type = ((ITypeBinding) type).getComponentType();
				}
				
				Clazz c1 = null;
				if (classList.containsKey(type.getName())) {
					c1 = classList.get(type.getName());
				} else {
					c1 = new Clazz(type.getName());
					c1.setTag(tag++);
					classList.put(type.getName(), c1);
				}

				Clazz c2 = null;
				TypeDeclaration td = (TypeDeclaration) node.getParent();
				if (classList.containsKey(td.getName().getIdentifier())) {
					c2 = classList.get(td.getName().getIdentifier());
				} else {
					c2 = new Clazz(td.getName().getIdentifier());
					c2.setTag(tag++);
					classList.put(td.getName().getIdentifier(), c2);
				}

				Logger.log("DPConnectionVisitor - " + c2.getName()
						+ " has a dependency to " + c1.getName());
				if (!c1.getInList().contains(c2)) {
					c1.getInList().add(c2);
					c2.getOutList().add(c1);
					edgeCount++;
					c1.getMetrics().Dep_In++;
					c2.getMetrics().Dep_Out++;
				}
				Logger.log("DPConnectionVisitor - " + c1.getInList());
				Logger.log("DPConnectionVisitor - " + c2.getOutList());
			}
		}		

		@Override
		public boolean visit(FieldDeclaration node) {
			try {
				// or it could be EnumDeclaration
				if (node.getParent() instanceof TypeDeclaration) {
					String fullyQualifiedName = getFullyQualifiedName((TypeDeclaration) node
							.getParent());
					Logger.log("DPConnectionVisitor - visit(FieldDeclaration node) - "
									+ fullyQualifiedName
									+ "Field: "
									+ node.getType());

					addConnection(node.getType(), node, "FieldDeclaration");
					
					TypeDeclaration td = (TypeDeclaration) node.getParent();
					if (classList.containsKey(td.getName().getIdentifier())) {
						Clazz c = classList.get(td.getName().getIdentifier());
						c.getMetrics().NumAttr++;
					}					
				}
			} catch (Exception ex) {
				Logger.log(ex);
			}

			return super.visit(node);
		}
		
		@Override
		public boolean visit(MethodDeclaration node) {
			try {
				if (node.getParent() instanceof TypeDeclaration) {
					String fullyQualifiedName = getFullyQualifiedName((TypeDeclaration) node
							.getParent());
					Logger.log("DPConnectionVisitor - visit(MethodDeclaration node) - "
									+ fullyQualifiedName
									+ " Method: "
									+ node.getName()
									+ " Params: "
									+ node.parameters()
									+ " Returns: "
									+ node.getReturnType2());

					for (int i = 0; i < node.parameters().size(); i++) {
						Object obj = node.parameters().get(i);
						if (obj instanceof SingleVariableDeclaration) {
							Type type = ((SingleVariableDeclaration) obj)
									.getType();
							addConnection(type, node, "MethodDeclaration");
						}
					}
					
					if (!node.isConstructor()) {
						TypeDeclaration td = (TypeDeclaration) node.getParent();
						if (classList.containsKey(td.getName().getIdentifier())) {
							Clazz c = classList.get(td.getName()
									.getIdentifier());

							for (int i = 0; i < node.modifiers().size(); i++) {
								Object obj = node.modifiers().get(i);
								if (obj instanceof Modifier) {
									Modifier m = (Modifier) obj;
									if (m.isPublic()) {
										c.getMetrics().NumPubOps++;
										break;
									}
								}
							}

							if (node.getName().getIdentifier()
									.startsWith("get")) {
								c.getMetrics().Getters++;
							}

							if (node.getName().getIdentifier()
									.startsWith("set")) {
								c.getMetrics().Setters++;
							}

							c.getMetrics().NumOps++;
						}
					}
				}
			} catch (Exception ex) {
				Logger.log(ex);
			}
			return super.visit(node);
		}
		
		@Override
		public boolean visit(MethodInvocation node) {
			try {
				if (node.getExpression() != null) {
					ITypeBinding typeBinding = node.getExpression()
							.resolveTypeBinding();
					Logger.log("MethodInvocation " + node.toString()
							+ " " + typeBinding.getName());
					ASTNode parent = node.getParent();
					while (!(parent.getParent() instanceof TypeDeclaration)) {
						parent = parent.getParent();
					}
					addConnection(typeBinding, parent);
				}
			} catch (Exception ex) {
				Logger.log(ex);
			}
		    
			return super.visit(node);
		}
		
		@Override
		public boolean visit(ClassInstanceCreation node) {
			try {
				Logger.log("ClassInstanceCreation " + node.toString());
				Type type = node.getType();
				ASTNode parent = node.getParent();
				while (!(parent.getParent() instanceof TypeDeclaration)) {
					parent = parent.getParent();
				}
				addConnection(type, parent, "ClassInstanceCreation");
			} catch (Exception ex) {
				Logger.log(ex);
			}
			
			return super.visit(node);
		}
	}

}
