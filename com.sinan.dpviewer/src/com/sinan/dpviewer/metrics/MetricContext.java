package com.sinan.dpviewer.metrics;

public class MetricContext {
	
	public int NumAttr = 0;
	public int NumOps = 0;
	public int NumPubOps = 0;	
	public int Setters = 0;
	public int Getters = 0;
	public int Dep_In = 0;
	public int Dep_Out = 0;
	public int EC_Attr = 0;
	public int IC_Attr = 0;
	public int EC_Par = 0;
	public int IC_Par = 0;

}
